#!/bin/bash
#include <stdio.h>

#ifdef fakeshell
source my-bash-code-kinda-looks-like-c
#endif

int
main () {

    /* made by izabera
     *
     * this script/program/whatever has no real purpose
     * other than showing that bash can be as c-ish as csh */

    // first declare the variables
    int x = 3;
    int y = x + 2;

    char firstname[100];
    char lastname[100];

    // then perform advanced math on them
    y += x * 3;
    x ++;

    x+=5;

    if ( x == 9 && y == 14 && x != y )
    then
        printf ("x is %d and y is %d and a string: %s\n", x, y, "this is a string");
        ++ x;
        y /= 2;
        printf ("x is %d and y is %d and a number: %d\n", x, y, 42);
    fi

    printf ("enter your full name: ");
    scanf ("%s %s", &firstname, &lastname);
    printf ("hello, \"%s %s\"\n", firstname, lastname);
}
