#!/bin/bash

mapfile -t _source < "${BASH_SOURCE[1]}"

# enable c-style comments
set -f
/* () { :; }
shopt -s expand_aliases extglob
alias *=:
// () { :; }


# int creates both a variable and a function
# creating the variable is not really necessary but...
# x += y calls the function x with arguments += y
# basically we just put it in a math context
int () {
  if (( $# == 0 )); then return; fi
  declare -gi "$1"
  eval "
    $1 () (( $1 \"\$@\" ))
    $1-- () (( $1 -- ))
    --$1 () (( $1 -- ))
    $1++ () (( $1 ++ ))
    ++$1 () (( $1 ++ ))
  "
  if (( $# > 1 )); then "$@"; fi
}

++ () (( $1 ++ ))
-- () (( $1 -- ))

# handle x=y+z
command-not-found-handler () (( "$@" ))



char () {
  # the idea is the same as int
  if (( $# == 0 )); then return; fi
  eval "
    $1 () {
      case \$1 in
        =) $1=\$2 ;;
        ==) if [[ -v \$2 ]]; then
              [[ \$$1 == \"\${!2}\" ]]
            else
              [[ \$$1 == \"\$2\" ]]
            fi ;;
      esac
    }
  "
  if (( $# > 1 )); then "$@"; fi
}






# a (fragile) c-style printf

# the magicalias hack
alias printf="_printf #"

# there must be a space between printf and (
# it can't span multiple lines
# all the arguments must be followed by a ,
# and there can be no space between the argument and the ,
_printf () {
  local arg args lastarg
  # get that line, remove the printf
  read -r _ arg <<< "${_source[BASH_LINENO - 1]}"
  # great, the arguments match the array syntax
  eval "args=$arg"
  # now there are extra commas, we must remove them
  # the last argument can be a string that ends in a comma, keep it verbatim
  lastarg=${args[-1]}
  unset "args[-1]"
  args=("${args[@]%,}" "$lastarg")
  for ((arg = 1; arg < ${#args[@]}; arg ++)); do
    # if the argument is a variable name, replace it with its content
    # if it's a number or any other string, leave it as is
    # this means you can't printf a string that matches a variable name :\
    if [[ ${args[arg]} = *[!0-9]* && -v ${args[arg]} ]]; then
      args[arg]=${!args[arg]}
    fi
  done
  builtin printf "${args[@]}"
}



# scanf is basically an interface to read
# arrays are completely fake
alias scanf="_scanf #"
_scanf () {
  local arg args lastarg
  read -r _ arg <<< "${_source[BASH_LINENO - 1]}"
  # eval it without the &
  eval "args=${arg//&}"
  lastarg=${args[-1]}
  unset "args[-1]"
  args=("${args[@]%,}" "$lastarg")
  # just throw the format away and hope it's fine
  # and remove everything after the last character in IFS
  read -r "${args[@]:1}" _
}





trap main EXIT
